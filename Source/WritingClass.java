package com.assignment2.i_o;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.assignment2.model.Scheduler;
import com.assignment2.model.Server;
import com.assignment2.model.Task;

public class WritingClass {

	public static void printServersAndContents(Scheduler scheduler, PrintWriter printWriter) {

		for (Server i : scheduler.getServers()) {//se itereaza prin servere

			printWriter.printf("queue %d", i.getId());
			for (Task t : i.getTasks()) {//se itereaza prin clientii fiecarui server
				printWriter.printf(" (%d , %d , %d)", t.getId(), t.getArrivalTime(), t.getProcessingTime());
			}
			printWriter.printf("%n");
			printWriter.flush();
		}

	}

	public static void printAwaitingTasks(List<Task> generatedTasks, PrintWriter pw) {
		if (!generatedTasks.isEmpty()) {//daca lista nu e goala
			pw.print("Awaiting clients are:");//se afiseaza clientii
			for (Task i : generatedTasks) {
				pw.printf("(%d ,%d ,%d )", i.getId(), i.getArrivalTime(), i.getProcessingTime());
			}
		} else
			pw.print("No clients waiting");
		pw.println();

	}

	public static PrintWriter openFileToWrite(String pathToOutFile) {
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(pathToOutFile);//se instantiaza un filewriter folosind calea specificata
			PrintWriter printWriter = new PrintWriter(fileWriter);
			return printWriter;

		} catch (IOException e) {
			System.out.println("Invalid output file!");//daca se intampla cv =>mesaj
		}
		return null;
	}
}
