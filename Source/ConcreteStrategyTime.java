package com.assignment2.strategies;

import java.util.Collections;
import java.util.List;

import com.assignment2.model.Server;
import com.assignment2.model.Task;

public class ConcreteStrategyTime implements Strategy {
	
	public void addTask(List<Server> servers, Task t) {
		Collections.sort(servers, Server.serverWaitingTime);
		servers.get(0).addTask(t);
		
	}
	

}
