package com.assignment2.strategies;

import java.util.List;

import com.assignment2.model.Server;
import com.assignment2.model.Task;

public interface Strategy {
	public enum SelectionPolicy{
		SHORTEST_QUEUE,SHORTEST_TIME;
	}
	
	public void addTask(List<Server> servers,Task t);
}
