package com.assignment2.model;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.assignment2.i_o.WritingClass;

public class SimulationManager implements Runnable {
	public int timeLimit = 15;
	public int maxProcessingTime = 10;
	public int minProcessingTime = 2;
	public int maxArrivalTime = timeLimit;
	public int minArrivalTime = 0;
	public int numberOfServers = 3;
	public int numberOfClients = 10;
	public String outPath;//calea spre fisierul de iesire
	public float waitingTimeMed = 0;

	private Scheduler scheduler;

	private List<Task> generatedTasks;

	public SimulationManager(int timeLimit, int maxProcessingTime, int minProcessingTime, int maxArrivalTime,
			int minArrivalTime, int numberOfServers, int numberOfClients, String path) {

		this.timeLimit = timeLimit;//initializare date 
		this.maxProcessingTime = maxProcessingTime;//(datele care vor fi citite din fisierul de intrare
		this.minProcessingTime = minProcessingTime;
		this.maxArrivalTime = maxArrivalTime;
		this.minArrivalTime = minArrivalTime;
		this.numberOfServers = numberOfServers;
		this.numberOfClients = numberOfClients;
		this.outPath=path;
		scheduler = new Scheduler(numberOfServers);// creare Obiect Scheduler
		for (Server server : scheduler.getServers()) {
			Thread t = new Thread(server);
			t.start();//pt fiecare coada se creeaza un thread nou si se porneste
		}
		generatedTasks = new ArrayList<Task>();//se initializeaza coada de clienti 
		generateNRandomTasks();//se genereaza random clientii
	}

	private void generateNRandomTasks() {//generarea clientilor random
		Random r = new Random();//creem un obiect nou 
		for (int i = 1; i <= numberOfClients; i++) {
			Task n = new Task();//se creeaza un nou client
			n.setId(i);
			n.setArrivalTime(r.nextInt(maxArrivalTime - minArrivalTime-1) + minArrivalTime+1);//arrival time e generat random intre val specificate
			n.setProcessingTime(r.nextInt(maxProcessingTime - minProcessingTime-1) + minProcessingTime+1);//la fel si processingTime
			generatedTasks.add(n);//clientul e adaugat in lista

		}
		Collections.sort(generatedTasks);//se sorteaza clientii in fc de arrivalTime
	}

	public boolean closeServersIfNoMoreClients() {//functie folosita pentru a inchide mai devreme cozile daca nu mai sunt clienti care 
		if (generatedTasks.isEmpty()) {//asteapta 
			int ok = 0;
			for (Server i : scheduler.getServers()) {
				if (!i.getTasks().isEmpty())
					ok = 1;
			}
			if (ok == 0) {
				scheduler.closeAll();
				return true;
			}
		}
		return false;

	}

	public void run() {
		int currentTime = 0;//va tine timpul simularii
		int nr = 0;//va numara clientii care sunt asezati la coada si au timp sa si termine cumparaturile pana la finalul simularii
		PrintWriter pw = WritingClass.openFileToWrite(this.outPath);//cu acest obiect se va scrie in fisier
		pw.printf("Time=%d %n", currentTime);
		while (currentTime < timeLimit && closeServersIfNoMoreClients() == false) {//acest while tine thread ul activ
			for (Iterator<Task> iterator = generatedTasks.iterator(); iterator.hasNext();) {
				Task task = (Task) iterator.next();//se itereaza prin clientii care sunt in magazin
				if (task.getArrivalTime() == currentTime) {
					scheduler.dispatchTask(task);//se adauga la coada potrivita clientul(in functie de strategie)
					if (task.getProcessingTime() + currentTime <= timeLimit) {
						waitingTimeMed += scheduler.getServers().get(0).getWaitingPeriod().get();
						nr++;//se pun la calcul doar clientii care au timp sa si termine cumparaturile 
					}
					iterator.remove();//se sterge din lista clientul pus la coada
				}
			}
			WritingClass.printAwaitingTasks(generatedTasks, pw);//afisare clienti care asteapta in magazin
			WritingClass.printServersAndContents(scheduler, pw);//afisare cozi
			pw.println();
			currentTime++;//creste timpul de simulare
			try {
				Thread.sleep(1000);
				pw.printf("Time=%d %n", currentTime);//afisare timp curent
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		waitingTimeMed /= nr;//se calculeaza timpul mediu de asteptare
		scheduler.closeAll();//se inchid cozile
		pw.printf("all queues closed!%nAverage waiting time is %.2f", waitingTimeMed);
		pw.close();//se inchide print writer ul
	}
}
