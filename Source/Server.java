package com.assignment2.model;

import java.util.Comparator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
	private int id;
	private BlockingQueue<Task> tasks;
	private AtomicInteger waitingPeriod;
	public AtomicBoolean running = new AtomicBoolean(true);//flag pt inchidere/deschidere coada
	
	public BlockingQueue<Task> getTasks() {
		return tasks;
	}

	public void setTasks(BlockingQueue<Task> tasks) {
		this.tasks = tasks;
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public static Comparator<Server> serverWaitingTime = new Comparator<Server>() {//l am implementat pt sortare in functie de timpul de asteptare 
		public int compare(Server s1, Server s2) {
			Integer serverWaitingTime1 = s1.getWaitingPeriod().get();
			Integer serverWaitingTime2 = s2.getWaitingPeriod().get();
			return serverWaitingTime1.compareTo(serverWaitingTime2);
		}
	};

	public static Comparator<Server> serverNumberOfTasks = new Comparator<Server>() {//pt sortare in functie de numarul de clienti care
		public int compare(Server s1, Server s2) {//asteapta la coada
			Integer serverNumberOfTasks1 = s1.getTasks().size();
			Integer serverNumberOfTasks2 = s2.getTasks().size();
			return serverNumberOfTasks1.compareTo(serverNumberOfTasks2);
		}
	};

	public Server() {
		LinkedBlockingQueue<Task> tasks = new LinkedBlockingQueue<Task>();//se initializeaza coada de clienti
		this.setTasks(tasks);
		AtomicInteger wait = new AtomicInteger();//se initializeaza timpul de asteptare
		this.setWaitingPeriod(wait);
	}

	public void addTask(Task newTask) {//se adauga un client la coada si se actualizeaza timpul de asteptare
		tasks.add(newTask);
		waitingPeriod.addAndGet(newTask.getProcessingTime());
	}

	public void stop() {//se foloseste pentru a opri manual thread ul care proceseaza clientii
		running.getAndSet(false);
	}

	public void run() {
		try {
			while (running.get()) {//daca flag ul e setat pe true
				while (!tasks.isEmpty()) {//cat timp coada de task uri nu e goala
					Thread.sleep(1000);
					tasks.peek().setProcessingTime(tasks.peek().getProcessingTime() - 1);//scade timpul de procesare al clientului din vf cozii
					waitingPeriod.decrementAndGet();//scade perioada de asteptare
					if (tasks.peek().getProcessingTime() == 0)
						tasks.poll();//daca primul client si a terminat tp de asteptare, e scos de la coada
				}

			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
