package com.assignment2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.assignment2.i_o.ReadingClass;

public class App {
	
	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(new File(args[0]));//deschidere fisier citire
			if(sc!=null) {
				ReadingClass.readInputData(sc,args[1]);//se citesc datele
				sc.close();
			}
		} catch (FileNotFoundException e) {
			System.out.println("Invalid file path!");
		}
		
		
	}
}
