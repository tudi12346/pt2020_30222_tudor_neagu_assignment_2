package com.assignment2.i_o;

import java.util.Scanner;

import com.assignment2.model.SimulationManager;

public class ReadingClass {
	
	
	public static boolean readInputData(Scanner sc,String path) {
		int timeLimit;
		int maxProcessingTime;
		int minProcessingTime;
		int maxArrivalTime;
		int minArrivalTime;
		int numberOfServers;
		int numberOfClients;
		if(sc!=null) {
			try {//se incearca citirea din fisier a datelor
			numberOfClients=Integer.parseInt(sc.nextLine());
			numberOfServers=Integer.parseInt(sc.nextLine());
			timeLimit=Integer.parseInt(sc.nextLine());
			String[] line = sc.nextLine().split(",");
			minArrivalTime=Integer.parseInt(line[0]);
			maxArrivalTime=Integer.parseInt(line[1]);
			line = sc.nextLine().split(",");//se imparte linia dupa virgula
			minProcessingTime=Integer.parseInt(line[0]);
			maxProcessingTime=Integer.parseInt(line[1]);
			SimulationManager gen =new SimulationManager(timeLimit, maxProcessingTime, minProcessingTime, maxArrivalTime, minArrivalTime, numberOfServers, numberOfClients,path);
			Thread t=new Thread(gen);
			t.start();
			return true;
			}catch (Exception e) {
				System.out.println("Invalid data inside input file!");
				return false;
			}
		}
		return false;	
	}
}
