package com.assignment2.model;

import java.util.ArrayList;
import java.util.List;

import com.assignment2.strategies.ConcreteStrategyQueue;
import com.assignment2.strategies.ConcreteStrategyTime;
import com.assignment2.strategies.Strategy;
import com.assignment2.strategies.Strategy.SelectionPolicy;

public class Scheduler {
	private List<Server> servers;
	private int maxNoServers;
	private Strategy strategy;

	public Scheduler(int maxNoServers) {
		// for maxNoServers
		// create server object
		servers = new ArrayList<Server>();//init lista servere
		for (int i = 1; i <= maxNoServers; i++) {
			Server x = new Server();//creare servere
			x.setId(i);//se numeroteaza serverele
			this.servers.add(x);//se adauga la lista
		}
		strategy = new ConcreteStrategyTime();//se creeaza o strategie noua 

	}

	public void changeStrategy(SelectionPolicy policy) {
		// apply strategy pattern to instantiate the strategy
		// with the concrete strategy corresponding to
		// policy
		if (policy == SelectionPolicy.SHORTEST_QUEUE) {
			strategy = new ConcreteStrategyQueue();
		}
		if (policy == SelectionPolicy.SHORTEST_TIME) {
			strategy = new ConcreteStrategyTime();
		}
	}

	public void dispatchTask(Task t) {
		strategy.addTask(servers, t);//in functie de strategie sa adauga clientul la coada coresp
	}

	public List<Server> getServers() {
		return servers;
	}

	public void setServers(List<Server> servers) {
		this.servers = servers;
	}

	public int getMaxNoServers() {
		return maxNoServers;
	}

	public void setMaxNoServers(int maxNoServers) {
		this.maxNoServers = maxNoServers;
	}

	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	public void closeAll() {//folosit pt inchiderea severelor 
		for (Server s : this.getServers()) {
			s.stop();
		}
	}

}
